{Atom} = require './atom'
{
  AlExpr,
  AssignExpr,
  BlockExpr,
  BooleanExpr,
  CallExpr,
  ClassExpr,
  DefExpr,
  IfExpr,
  MemberExpr,
  NameExpr,
  NullExpr,
  NumberExpr,
  StringExpr,
  UndefinedExpr,
} = require './ast'
{all, map} = require 'prelude-ls'

export class Parser
  (specialForms) ->
    @specialForms = Object.create(null)
    @specialForms <<< specialForms

  parse: (sexpr) ->
    switch
    | typeof sexpr == 'boolean' => new BooleanExpr(sexpr)
    | sexpr instanceof Array =>
        if sexpr.length == 0
          throw Error('call must have a callee')
        if (sexpr[0] instanceof Atom
            && sexpr[0].name of @specialForms)
          @specialForms[sexpr[0].name](@, sexpr.slice(1))
        else
          new CallExpr(@parse(sexpr[0]), sexpr.slice(1).map(@~parse))
    | sexpr instanceof Atom => new NameExpr(sexpr.name)
    | sexpr === null => new NullExpr()
    | typeof sexpr == 'number' => new NumberExpr(sexpr)
    | typeof sexpr == 'string' => new StringExpr(sexpr)
    | sexpr === undefined => new UndefinedExpr()
    | otherwise => throw Error('cannot parse sexpr')

export
  '.': (parser, args) ->
    new MemberExpr(parser.parse(args[0]), args[1].name)

  al: (parser, args) ->
    params = [args[0] |> map (.name)]
    body = parser.parse([new Atom('block'), args[1]])
    new AlExpr(params, body)

  assign: (parser, args) ->
    new AssignExpr(parser.parse(args[0]), parser.parse(args[1]))

  block: (parser, args) ->
    if (args.length == 1
        && args[0] instanceof Array
        && args[0][0] instanceof Atom
        && args[0][0].name == 'do')
      new BlockExpr(args[0].slice(1).map(parser~parse))
    else
      throw Error('block takes a do-block')

  class: (parser, args) ->
    base = parser.parse(args[0])

    methods = []
    if (args.length == 2
        && args[1] instanceof Array
        && args[1][0] instanceof Atom
        && args[1][0].name == 'do')
      for method in args[1].slice(1)
        if (method instanceof Array
            && method[0] instanceof Atom)
          switch method[0].name
          | 'defnew' =>
              methods.push({
                type: 'constructor',
                params: method.slice(1, method.length - 1).map((.name)),
                body: parser.parse([new Atom('block'), method[method.length - 1]]),
              })
          | otherwise =>
              throw Error('class can only contain method definitions')
        else
          throw Error('class can only contain method definitions')
    else
      throw Error('class takes a do-block')

    new ClassExpr(base, methods)

  def: (parser, args) ->
    if args.length != 2
      throw Error('def takes a name and a value')

    if (args[0] instanceof Atom)
      new DefExpr(args[0].name, parser.parse(args[1]))
    else
      throw Error('this is not a name')

  defal: (parser, args) ->
    if args.length != 2
      throw Error('defal takes a signature and a do-block')

    parser.parse([
      new Atom('def'),
      args[0][0],
      [
        new Atom('al'),
        args[0].slice(1),
        args[1],
      ],
    ])

  defclass: (parser, args) ->
    if args.length != 2
      throw Error('defclass takes a signature and a do-block')

    parser.parse([
      new Atom('def'),
      args[0][0],
      [
        new Atom('class'),
        args[0][1],
        args[1],
      ],
    ])

  if: (parser, args) ->
    unless args.length in [2, 3]
      throw Error('if takes a then-block and optionally an else-block')

    if (args[1] instanceof Array
        && args[1][0] instanceof Atom
        && args[1][0].name == 'then')
      consequence = new BlockExpr(args[1].slice(1).map(parser~parse))
    else
      throw Error('if takes a then-block')

    if args.length == 3
      if (args[2] instanceof Array
          && args[2][0] instanceof Atom
          && args[2][0].name == 'else')
        alternative = new BlockExpr(args[2].slice(1).map(parser~parse))
      else
        throw Error('if takes a else-block')
    else
      alternative = parser~parse(undefined)

    new IfExpr(parser.parse(args[0]), consequence, alternative)
