Atom = require('../lib/atom').Atom
compile = require '../lib/compile'
escodegen = require 'escodegen'
fs = require 'fs'
parse = require '../lib/parse'
read = require('../lib/reader').parse
util = require 'util'

code = '
defal log(msg) do
  console.log(msg);
end;

log(#t);
log #t;
log(not #t);
log(#t, #f);
if(#t) then
  log("true");
  log(#t, #f);
end;

if #t then
  log("hello");
  log(not #t, not #f);
else
  log "cannot happen";
  log("lol", 42);
end;

try() do
  throw Error("EPIC");
catch
  case _ do
    throw Error("FUCKING");
  end;
finally
  throw Error("FAILURE");
end;

defal warn(msg) do
  log(join(" ", map(list("warning: ", msg), al(x) do
    toUpperCase(x);
  end)));
end;

defal main() do
  warn("Hello, world!");
end;

defclass A(Object) do
  defnew(a, b, c) do
    assign(this.a, a);
    assign(this.b, b);
    assign(this.c, c);
  end;
end;
'
sexpr = read(if process.argv.length == 2
  then code
  else fs.readFileSync(process.argv[2], 'utf-8'))

console.log(util.inspect(sexpr, {depth: null}))

console.log('-' * 80)

parser = new parse.Parser({
    '.': parse['.'],
    al: parse.al,
    assign: parse.assign,
    block: parse.block,
    class: parse.class,
    def: parse.def,
    defal: parse.defal,
    defclass: parse.defclass,
    if: parse.if,
})
ast = parser.parse(sexpr)

console.log(ast.toString())

console.log('-' * 80)

esAST = {
  type: 'Program',
  body: [
    {type: 'ExpressionStatement', expression: {type: 'Literal', value: 'use strict'}},
  ] ++ compile.toStmts((e) -> [{type: 'ExpressionStatement', expression: e}], ast),
}

console.log(escodegen.generate(esAST))
