{
  AlExpr,
  AssignExpr,
  BlockExpr,
  BooleanExpr,
  CallExpr,
  ClassExpr,
  DefExpr,
  IfExpr,
  MemberExpr,
  NameExpr,
  NullExpr,
  NumberExpr,
  StringExpr,
  UndefinedExpr,
} = require './ast'
{concatMap, initial, last, map} = require 'prelude-ls'

identifier = (name) ->
  {type: 'Identifier', name: name}

literal = (value) ->
  if value === undefined
    {
      type: 'UnaryExpression',
      operator: 'void',
      prefix: true,
      argument: literal(0),
    }
  else
    {type: 'Literal', value: value}

exprStmt = (expr) ->
  {type: 'ExpressionStatement', expression: expr}

exprStmts = (expr) ->
  [exprStmt(expr)]

returnStmt = (expr) ->
  {type: 'ReturnStatement', argument: expr}

returnStmts = (expr) ->
  [returnStmt(expr)]

blockStmt = (stmts) ->
  {type: 'BlockStatement', body: stmts}

assertBoolean = (expr) ->
  {
    type: 'CallExpression',
    callee: identifier('__assertBoolean'),
    arguments: [expr],
  }

export toStmts = (result, expr) -->
  switch
  | expr instanceof AssignExpr =>
      if expr.target instanceof MemberExpr
        target = {
          type: 'MemberExpression',
          computed: false,
          object: toExpr(expr.target.object),
          property: identifier(expr.target.member),
        }
      else
        target = toExpr(expr.target)
      exprStmts({
        type: 'AssignmentExpression',
        operator: '=',
        left: target,
        right: toExpr(expr.value),
      }) ++ result(literal(undefined))
  | expr instanceof BlockExpr =>
      if expr.body.length == 0
        result(literal(undefined))
      else
        i = expr.body |> initial |> concatMap(toStmts(exprStmts))
        l = expr.body |> last |> toStmts(result)
        i ++ l
  | expr instanceof CallExpr =>
      result(toExpr(expr))
  | expr instanceof DefExpr =>
      [{
        type: 'VariableDeclaration',
        kind: 'var',
        declarations: [{
          type: 'VariableDeclarator',
          id: identifier(expr.name),
          init: toExpr(expr.value),
        }],
      }] ++ result(literal(undefined))
  | expr instanceof NullExpr =>
      result(toExpr(expr))
  | expr instanceof IfExpr =>
      condition = expr.condition |> toExpr
      consequent = expr.then |> toStmts(result)
      alternate = expr.else |> toStmts(result)
      [{
        type: 'IfStatement',
        test: assertBoolean(condition),
        consequent: blockStmt(consequent),
        alternate: blockStmt(alternate),
      }]
  | expr instanceof UndefinedExpr =>
      result(toExpr(expr))
  | otherwise =>
      throw Error('cannot compile ' + expr + ' to stmts')

export toExpr = (expr) ->
  switch
  | expr instanceof AlExpr =>
      {
        type: 'FunctionExpression',
        id: null,
        params: expr.params.map(identifier),
        defaults: [],
        rest: null,
        body: blockStmt(toStmts(returnStmts, expr.body)),
        generator: false,
        expression: false,
      }
  | expr instanceof BooleanExpr =>
      literal(expr.value)
  | expr instanceof CallExpr =>
      callee = expr.callee |> toExpr
      args = expr.args |> map(toExpr)
      {type: 'CallExpression', callee: callee, arguments: args}
  | expr instanceof ClassExpr =>
      {
        type: 'ClassExpression',
        id: null,
        superClass: toExpr(expr.base),
        body: {
          type: 'ClassBody',
          body: expr.methods.map((method) ->
                  switch method.type
                  | 'constructor' =>
                      {
                        type: 'MethodDefinition',
                        kind: 'constructor',
                        key: identifier('constructor'),
                        value: {
                          type: 'FunctionExpression',
                          id: null,
                          params: method.params.map(identifier),
                          defaults: [],
                          rest: null,
                          body: blockStmt(toStmts(exprStmts, method.body)),
                          generator: false,
                          expression: false,
                        },
                        computed: false,
                        static: false,
                      }
                  | otherwise => throw Error('cannot compile method of type')),
        },
      }
  | expr instanceof MemberExpr =>
      {
        type: 'CallExpression',
        callee: identifier('__readProperty'),
        arguments: [toExpr(expr.object), literal(expr.member)],
      }
  | expr instanceof NameExpr =>
      identifier(expr.name)
  | expr instanceof NullExpr =>
      literal(null)
  | expr instanceof NumberExpr =>
      literal(expr.value)
  | expr instanceof StringExpr =>
      literal(expr.value)
  | expr instanceof UndefinedExpr =>
      literal(undefined)
  | otherwise =>
      throw Error('cannot compile ' + expr + ' to expr')
