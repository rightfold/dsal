{
var Atom = require('./atom').Atom;
}

program
    = body:block { return [new Atom('block'), [new Atom('do')].concat(body)]; }

block
    = (e:expr SEMICOLON { return e; })*

expr
    = singleCallExpr

singleCallExpr
    = callee:multiCallExpr arg:multiCallExpr? blockCalls:blockCalls
        {
            var call = arg === null ? callee : [callee].concat([arg]);
            return blockCalls.length === 0 ? call : call.concat(blockCalls);
        }

multiCallExpr
    = callee:parenExpr calls:call*
        { return calls.reduce(function(callee, call) { return call(callee); }, callee); }

call
    = LPAREN initArgs:(e:expr COMMA { return e; })* lastArg:expr? RPAREN
        {
            return function(callee) {
                return [callee].concat(initArgs, lastArg === null ? [] : [lastArg]);
            };
        }
    / PERIOD member:ATOM
        { return function(callee) { return [new Atom('.'), callee, member]; }; }

blockCallIntro
    = DO
    / CATCH
    / ELSE
    / FINALLY
    / THEN

blockCalls
    = calls:(intro:blockCallIntro block:block { return [intro].concat(block); })+ END
        { return calls; }
    / '' { return []; }

parenExpr
    = LPAREN expr:expr RPAREN { return expr; }
    / primaryExpr

primaryExpr
    = ATOM
    / TRUE
    / FALSE
    / NUMBER
    / STRING

_ = [ \t\r\n]*

LPAREN = _ '(' _
RPAREN = _ ')' _
SEMICOLON = _ ';' _
COMMA = _ ',' _
PERIOD = _ '.' _

ATOM = _ !KEYWORD name:$([a-zA-Z_$]+) _
        { return new Atom(name); }
TRUE = _ '#t' _ { return true; }
FALSE = _ '#f' _ { return false; }
NUMBER = _ text:$([0-9]+) _ { return +text; }
STRING = _ '"' text:$([^"]*) '"' _ { return text; }

KEYWORD = 'catch' / 'do' / 'else' / 'end' / 'finally' / 'then'

CATCH = _ 'catch' _ { return new Atom('catch'); }
DO = _ 'do' _ { return new Atom('do'); }
ELSE = _ 'else' _ { return new Atom('else'); }
FINALLY = _ 'finally' _ { return new Atom('finally'); }
THEN = _ 'then' _ { return new Atom('then'); }

END = _ 'end' _
