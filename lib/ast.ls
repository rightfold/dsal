indent = (s) ->
  s.split('\n').map(('  ' +)).join('\n')

export class Expr

export class AlExpr
  (@params, @body) ->

  toString: ->
    r = "al(#{@params.join(', ')}) is\n"
    r += indent("#{@body}")
    r += '\nend'
    r

export class AssignExpr
  (@target, @value) ->

  toString: ->
    "set(#{@target}, #{@value})"

export class BlockExpr
  (@body) ->

  toString: ->
    r = 'block() do\n'
    for expr in @body
      r += indent("#{expr}") + '\n'
    r += 'end'
    r

export class BooleanExpr
  (@value) ->

  toString: ->
    if @value then '#t' else '#f'

export class CallExpr
  (@callee, @args) ->

  toString: ->
    "#{@callee}(#{@args.join(', ')})"

export class ClassExpr
  (@base, @methods) ->

  toString: ->
    r = "class(#{@base}) do\n"
    for method in @methods
      r += indent("#{method}\n")
    r += 'end'
    r

export class DefExpr
  (@name, @value) ->

  toString: ->
    "def(#{@name}, #{@value})"

export class IfExpr
  (@condition, @then, @else) ->

  toString: ->
    r = "if(#{@condition}) then\n"
    r += indent("#{@then}")
    r += "\nelse\n"
    r += indent("#{@else}")
    r += "\nend"
    r

export class MemberExpr
  (@object, @member) ->

  toString: ->
    "(#{@object}).#{@member}"

export class NameExpr
  (@name) ->

  toString: ->
    @name

export class NullExpr
  toString: ->
    '#n'

export class NumberExpr
  (@value) ->

  toString: ->
    "#{@value}"

export class StringExpr
  (@value) ->

  toString: ->
    JSON.stringify(@value)

export class UndefinedExpr
  toString: ->
    '#u'
